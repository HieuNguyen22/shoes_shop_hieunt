import 'package:app_shoes__shop/controllers/search_controller.dart';
import 'package:app_shoes__shop/models/product_model.dart';
import 'package:app_shoes__shop/pages/components/list_products_widget.dart';
import 'package:app_shoes__shop/pages/detail_page.dart';
import 'package:app_shoes__shop/ultilities/constants.dart';
import 'package:app_shoes__shop/ultilities/data.dart';
import 'package:app_shoes__shop/ultilities/flutter_icons.dart';
import 'package:app_shoes__shop/pages/components/banner_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:get/get.dart';

class SearchPage extends StatelessWidget {
  var searchController = Get.put(SearchController());
  bool isNavigatorCall = false;
  final textController = TextEditingController();

  SearchPage({Key? key, required this.isNavigatorCall}) : super(key: key);

  void _runFilter(String keyword) {
    List<ProductModel> results = [];
    if (keyword.isEmpty) {
      results = products;
    } else {
      results = products
          .where((product) => product.getName
              .toString()
              .toLowerCase()
              .contains(keyword.toLowerCase()))
          .toList();
    }

    searchController.foundProducts.value = results;
  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      body: getBody(size, context),
    );
  }

  Widget getBody(size, context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 0, bottom: 0, top: 16),
      child: ListView(
        physics: BouncingScrollPhysics(),
        scrollDirection: Axis.vertical,
        children: [
          (isNavigatorCall) ? getHeader(size) : Container(),
          getSearchBar(context),
          getResultRow(),
          SizedBox(height: 20),
          getResultProducts(size, context),
          SizedBox(height: 30),
        ],
      ),
    );
  }

  Widget getHeader(size) {
    return Padding(
      padding: const EdgeInsets.only(
        left: 16,
        bottom: 15,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            FlutterIcons.menu,
            color: Colors.black,
          ),
          SizedBox(height: 16),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                "Search",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 40,
                    color: Colors.black),
              ),
              Container(
                width: size.width / 5,
                padding: EdgeInsets.only(left: size.width / 5 * 0.1),
                color: primaryColor,
                child: IconButton(
                    onPressed: () {},
                    icon: Icon(FlutterIcons.search, color: primaryBgColor)),
              )
            ],
          ),
        ],
      ),
    );
  }

  Widget getSearchBar(context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Row(
        children: [
          // SizedBox(width: 5),
          (!isNavigatorCall)
              ? GestureDetector(
                  onTap: () {
                    searchController.keywordSearch.value = '';
                    searchController.foundProducts.value = products;
                    Navigator.of(context).pop();
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 5.0, right: 5),
                    child: Icon(Icons.arrow_back_ios),
                  ))
              : Container(),
          // SizedBox(width: 5),
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                border: Border.all(color: Colors.black, width: 1),
                borderRadius: BorderRadius.circular(15),
              ),
              child: TextFormField(
                onChanged: (value) {
                  searchController.keywordSearch.value = value;
                  _runFilter(value);
                },
                controller: textController,
                textInputAction: TextInputAction.done,
                cursorColor: primaryColor,
                decoration: InputDecoration(
                    filled: true,
                    fillColor: primaryBgColor,
                    // iconColor: Colors.black,
                    prefixIconColor: Colors.black,
                    contentPadding:
                        EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      borderSide: BorderSide.none,
                    ),
                    suffixIcon: GestureDetector(
                      onTap: () {
                        // return _runFilter(textController.text);
                      },
                      child: Icon(
                        Icons.search,
                        color: Colors.black54,
                        // size: 18,
                      ),
                    ),
                    hintText: "Type your keyword"),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget getResultRow() {
    return Obx(() => Padding(
        padding:
            const EdgeInsets.only(top: 15, left: 25, bottom: 10, right: 25),
        child: (!searchController.keywordSearch.value.isEmpty)
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    'Results for "${searchController.keywordSearch.value}"',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Text('${searchController.foundProducts.value.length} found',
                      style: TextStyle(fontWeight: FontWeight.bold)),
                ],
              )
            : Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Sale of the month',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(height: 20),
                  BannerSlider(imgList: imgListBannerCategory),
                  SizedBox(height: 20),
                  Text(
                    'Recent Searched Products',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ],
              )));
  }

  Widget getResultProducts(size, context) {
    return Expanded(
      child: Container(
        // height: size.height,
        // decoration: BoxDecoration(color: primaryBgColor),
        child: Obx(() => (!searchController.foundProducts.value.isEmpty)
            ? Center(
                child: ListProductsWidget(displayProducts: searchController.foundProducts.value)
              )
            : Padding(
                padding: const EdgeInsets.only(top: 40),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      width: size.width / 2,
                      height: size.height / 4,
                      decoration: BoxDecoration(
                          image: DecorationImage(
                              image: AssetImage(
                                  'assets/images/ic_not_found.png'))),
                    ),
                    Text('Not Found',
                        style: TextStyle(fontWeight: FontWeight.bold)),
                    Container(
                      width: size.width / 1.5,
                      child: Text(
                          'Sorry, the keyword you entered cannot be found. Please check again or search with another keyword.',
                          textAlign: TextAlign.center),
                    )
                  ],
                ),
              )),
      ),
    );
  }
}
