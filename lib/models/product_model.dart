import 'package:flutter/material.dart';

class ProductModel {
  int id;
  String name;
  String imagePath;
  String desc;
  String brand;
  Color color;
  int soldNum;
  int discount = 0;
  double price;

  ProductModel(
      {required this.id,
      required this.name,
      required this.imagePath,
      required this.desc,
      required this.brand,
      required this.color,
      required this.soldNum,
      required this.discount,
      required this.price});

  get getId => this.id;

  set setId(id) => this.id = id;

  get getName => this.name;

  set setName(name) => this.name = name;

  get getImageUrl => this.imagePath;

  set setImageUrl(imageUrl) => this.imagePath = imageUrl;

  get getDesc => this.desc;

  set setDesc(desc) => this.desc = desc;

  get getBrand => this.brand;

  set setBrand(brand) => this.brand = brand;

  get getColor => this.color;

  set setColor(color) => this.color = color;

  get getPrice => this.price;

  set setPrice(price) => this.price = price;

  String getInfo() {
    return "${this.id} - ${this.name} - ${this.price}";
  }
}
