import 'package:app_shoes__shop/models/cart_item_model.dart';
import 'package:app_shoes__shop/models/product_model.dart';

class CartModel {
  int id;
  DateTime time;
  List<CartItemModel> cart;

  CartModel(this.id, this.time, this.cart);

  get getId => this.id;

  set setId(id) => this.id = id;

  get getTime => this.time;

  set setTime(time) => this.time = time;

  get getCart => this.cart;

  set setCart(cart) => this.cart = cart;
}
