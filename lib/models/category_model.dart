class CategoryModel {
  int id;
  String name;
  String imgUrl;

  CategoryModel({required this.id, required this.name, required this.imgUrl});
  
  get getId => this.id;

  set setId(id) => this.id = id;

  get getName => this.name;

  set setName(name) => this.name = name;

  get getImgUrl => this.imgUrl;

  set setImgUrl(imgUrl) => this.imgUrl = imgUrl;
}
