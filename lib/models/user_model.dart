class UserModel {
  final String fullname;
  final String avatar;
  final String birthday;
  final String email;
  final String phoneNum;
  final String address;

  UserModel(
      this.fullname, this.avatar, this.birthday, this.email, this.phoneNum, this.address);
}
