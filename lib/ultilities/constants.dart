import 'dart:math';

import 'package:app_shoes__shop/models/cart_item_model.dart';
import 'package:app_shoes__shop/models/cart_model.dart';
import 'package:app_shoes__shop/models/category_model.dart';
import 'package:app_shoes__shop/models/customer_model.dart';
import 'package:app_shoes__shop/models/notification_model.dart';
import 'package:app_shoes__shop/models/product_model.dart';
import 'package:flutter/material.dart';

final Color blueColor = Color(0XFF0c99c3);
final Color greenColor = Color(0XFF3dc39d);
final Color yellowColor = Color(0XFFdac007);
final Color redColor = Color(0XFFbe0b2b);
final Color orangeColor = Color(0XFFbe740b);
final Color blackColor = Color.fromARGB(255, 17, 17, 16);
final Color greyColor = Color.fromARGB(255, 39, 39, 39);
final Color milkColor = Color.fromARGB(255, 186, 189, 0);
final Color lightGreenColor = Color.fromARGB(255, 61, 195, 79);
final Color strongBlueColor = Color.fromARGB(255, 0, 59, 168);
final Color vintageBlueColor = Color.fromARGB(255, 12, 27, 56);
final Color brownColor = Color.fromARGB(255, 102, 47, 27);
final Color purpleColor = Color.fromARGB(255, 46, 22, 110);

final Color primaryColor = Color.fromARGB(255, 55, 151, 98);
final Color primaryLightColor = Color.fromARGB(255, 137, 167, 150);
final Color primaryBgColor = Color.fromARGB(255, 236, 240, 236);